#!/bin/bash
#
# preprocess EPI images with fzj_mpi_parallel_simple.py (Jan Schreiber)
#
# load modules

source /data/inm1/mapping/software-2016b.source

# nifti data folder
SRC=/data/inm1/mapping/NaKo/Niftis2
# file identifier T1 T2
EXT1=_T1_3D_SAG_MPR_Tra.nii
EXT2=
# subject identifier
ID=??????
# name of batch text file
BATCH=ANAT_subs.txt
# number of tasks per node
TpN=48
# wall time
TIME=24:00:00

#	output folder
OUT=/work/jinm13/jinm1301/NaKo

if [ -f $BATCH ]; then
  rm $BATCH
fi
cnt=0
for subject in $SRC/$ID; do
  sub=$(basename $subject)
  T1RAW=$SRC/$sub/${sub}${EXT1}
  T2RAW=$SRC/$sub/${sub}${EXT2}
  if [ -f $T1RAW ] && [ ! -f $OUT/$sub/struc/T1_MNI152_2mm.nii.gz ]; then
      # subject ID
      echo "./pline_ANAT.sh $sub $T1RAW $OUT $T2RAW" >> $BATCH
      echo " $cnt + 1 " | bc
      cnt=$(  echo " $cnt + 1 " | bc)
    fi
  done

  echo " $cnt unprocessed data sets "

# process_batch_job_parallel  my-batch-file.txt  tasks-per-node  00:30:00

function process_batch_job_parallel() {
    BATCH_NAME=$1
    NUM_TASKS_PER_NODE=$2
    TIME=$3
    if [[ -s $BATCH_NAME ]]; then
            SLURM_OUT=${BATCH_NAME/.txt}_out.%j
            SLURM_ERR=${BATCH_NAME/.txt}_err.%j
            MAIL_USER=$(q_cpuquota | grep 'E-Mail:' | head -n 1 | tr -s ' ' | cut -d ' ' -f 2)
            # set maximal processing time
            if [[ -z $TIME ]]; then
                TIME=23:00:00
            fi
            # set number of tasks per node
            NUM_TASKS=$(cat $BATCH_NAME | wc -l)
            if [[ -z $NUM_TASKS_PER_NODE ]]; then
                NUM_TASKS_PER_NODE=24
            fi
            if [[ $NUM_TASKS_PER_NODE -gt $NUM_TASKS ]]; then
                NUM_TASKS_PER_NODE=$NUM_TASKS
            fi
            if [[ $NUM_TASKS_PER_NODE -lt 1 ]]; then
                NUM_TASKS_PER_NODE=1
            fi
            # compute number of cpu cores per task
            NUM_CPUS_PER_TASKS=$(echo "48 / $NUM_TASKS_PER_NODE" | bc)
            if [[ $NUM_CPUS_PER_TASKS -gt 24 ]]; then
                NUM_CPUS_PER_TASKS=24
            fi
            # set number of cpu cores per task in batch file
            sed -i $BATCH_NAME -e "s/NUM_CPUS_PER_TASKS/$NUM_CPUS_PER_TASKS/g"
            # compute number of nodes
            NUM_NODES=$(echo "$NUM_TASKS / $NUM_TASKS_PER_NODE " | bc)
            if [[ $NUM_NODES -lt 1 ]]; then
               NUM_NODES=1
            fi
            while [[ $NUM_NODES -gt 50 ]]; do
                NUM_NODES=$(echo "$NUM_NODES / 2 +1" | bc)
            done

            PARAMETERS="--mail-user=f.hoffstaedter@fz-juelich.de --mail-type=END --job-name=${BATCH_NAME/.txt} --nodes=$NUM_NODES --ntasks-per-node=$NUM_TASKS_PER_NODE --cpus-per-task=$NUM_CPUS_PER_TASKS --time=$TIME --partition="batch" --output=$SLURM_OUT --error=$SLURM_ERR"
            # PARAMETERS="--mail-user=f.hoffstaedter@fz-juelich.de --mail-type=END --job-name=${BATCH_NAME/.txt} --nodes=$NUM_NODES --ntasks-per-node=$NUM_TASKS_PER_NODE --cpus-per-task=$NUM_CPUS_PER_TASKS --time=$TIME --partition="mem256" --output=$SLURM_OUT --error=$SLURM_ERR"
            echo "srun $PARAMETERS python ${FZJDIR}/fzj_mpi_parallel_simple.py $BATCH_NAME"
            date --rfc-3339="seconds"
            # srun $PARAMETERS python $(pwd)/fzj_mpi_parallel_simple.py $BATCH_NAME
            date --rfc-3339="seconds"
    fi
}

# run batch
process_batch_job_parallel $BATCH $TpN $TIME
