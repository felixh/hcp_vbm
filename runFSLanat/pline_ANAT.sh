#!/bin/bash

##
#				Anatomical processing using FSL
#
#       Felix Hoffstaedter
#       Data and Platforms
#       INM-7 - Brain and Behaviour
#       September 2019 - Research Center Juelich
#
# 			use: execute pline_ANAT [ <subject_list> ]
##
subject=$1	# subject name
T1RAW=$2		# full path to T1 file
OUT=$3			# output directory
T2RAW=$4		# full path to T2 file
QC=$OUT/QC
CWD=$(pwd)

QCnew=yes #yes					# change to "yes" to redo quality control images

# functions:			prepro calls [ T1_anat T2_anat QC_T1 QC_T2 ]

if [ ! -d $OUT ]; then
	mkdir $OUT
fi

function prepro	{
	VBMDIR=$OUT/$1/struc
	CATDIR=$OUT/$1/CAT
	subLog=$OUT/$1_ANAT.log
	# start structural processing
	if [ -f $T1RAW ]; then
		date >> $subLog
		echo "Found T1, start preprocessing of $1" >> $subLog

		### T1 & T2 processing in parallel possible
		T1_anat $1
		T2_anat $1
		QC_T1 $1
		QC_T2 $1
		date
		echo "## $1 DONE"
	fi
}

### integrate run to log commands ---
run() {
  echo $@ >> $subLog
  $@
}


function T1_anat	{

	if [ -f $VBMDIR/GM_MNI152_1mm_mod_s3.nii.gz ] && [ -f $VBMDIR/pveseg_MNI152_2mm.nii.gz ]; then
		date >> $subLog; echo "- T1 preprocessing for $1 already performed"	>> $subLog
	else
		if [ -f $T1RAW ]; then
				if [ -f $VBMDIR/T1.anat/T1_biascorr_to_std_sub.mat ]; then
					date >> $subLog; echo "	- T1 Bias field correction, tissue segmentation & registration ready"	>> $subLog
				else
					date >> $subLog; echo "	T1 Bias field correction, tissue segmentation & registration"	>> $subLog
					mkdir -p $VBMDIR
					run fsl_anat --clobber -i $T1RAW -o $VBMDIR/T1
				fi
				# Files are found in T1.anat
				# - Reorientation and Cropping
				# If run, the original image (we shall call it T1 from here on in as an example), will be replaced by the reoriented and/or cropped version. The original versions are saved as files: T1_orig and T1_fullfov.
				# In addition, transformation files are provided to allow images to be moved between spaces: i.e. T1_orig2std.mat and T1_nonroi2roi.mat and their inverses and combinations.
				# - Bias-correction
				# The bias-corrected version of the image is called T1_biascorr.
				# - Registration and Brain-Extraction
				# The registration (to standard space) produces the following images that are in MNI space with a 2mm resolution:
				#     T1_to_MNI_lin (linear registration output)
				#     T1_to_MNI_nonlin (non-linear registration output)
				#     T1_to_MNI_nonlin_field (non-linear warp field)
				#     T1_to_MNI_nonlin_jac (Jacobian of the non-linear warp field)
				#     T1_vols.txt - a file containing a scaling factor and brain volumes, based on skull-contrained registration, suitable for head-size normalisation (as the scaling is based on the skull size, not the brain size)
				# The brain-extraction produces:
				#     T1_biascorr_brain
				#     T1_biascorr_brain_mask
				# - Segmentation
				# Tissue-type segmentation (done with FAST) produces:
				#     T1_biascorr - refined again in this stage
				#     T1_fast_pve_0, T1_fast_pve_1, T1_fast_pve_2 - partial volume segmentations (CSF, GM, WM respectively)
				#     T1_fast_pveseg - a summary image showing the tissue with the greatest partial volume fraction per voxel
				# - Subcortical segmentation (done with FIRST) produces:
				#     T1_subcort_seg - summary image of all subcortical segmentations
				#     all other outputs in the first_results subdirectory
				#     'T1_first_all_fast_firstseg - same as T1_subcort_seg`
				#     a host of other images relating to individual segmentations
				#     T1_biascorr_to_std_sub.mat (in the main anat directory) - a transformation matrix of the subcortical optimised MNI registration

				if [ ! -f $VBMDIR/subcort_seg_MNI152_2mm.nii.gz ]; then
					# write tissue segment volumes to file
					GMV=$(fslstats $VBMDIR/T1.anat/T1_fast_pve_1 -M -V | awk '{ print $1 * $3 }')
					WMV=$(fslstats $VBMDIR/T1.anat/T1_fast_pve_2 -M -V | awk '{ print $1 * $3 }')
					CSFV=$(fslstats $VBMDIR/T1.anat/T1_fast_pve_0 -M -V | awk '{ print $1 * $3 }')
					run echo -e "Vol__T1 \t $GMV \t $WMV \t $CSFV" >> $VBMDIR/vol_GM_WM_CSF.txt
					# gather usefull files
					run mv $VBMDIR/T1 $VBMDIR/T1_orig
					run imcp $VBMDIR/T1.anat/T1_biascorr $VBMDIR/T1
					run imcp $VBMDIR/T1.anat/T1_biascorr_brain $VBMDIR/T1_brain
					run imcp $VBMDIR/T1.anat/T1_biascorr_brain_mask $VBMDIR/T1_brain_mask
					run imcp $VBMDIR/T1.anat/T1_subcort_seg $VBMDIR/T1_subcort_seg
					run imcp $VBMDIR/T1.anat/T1_to_MNI_nonlin_field $VBMDIR/T1_to_MNI152_warp
					# warp T1 to MNI152 1mm & 2mm
					run applywarp -r $FSLDIR/data/standard/MNI152_T1_1mm_brain -i $VBMDIR/T1.anat/T1_biascorr_brain -o $VBMDIR/T1_MNI152_1mm -w $VBMDIR/T1.anat/T1_to_MNI_nonlin_field
					run applywarp -r $FSLDIR/data/standard/MNI152_T1_2mm_brain -i $VBMDIR/T1.anat/T1_biascorr_brain -o $VBMDIR/T1_MNI152_2mm -w $VBMDIR/T1.anat/T1_to_MNI_nonlin_field
					# warp GM 1mm & 2mm
					run applywarp -r $FSLDIR/data/standard/MNI152_T1_1mm_brain -i $VBMDIR/T1.anat/T1_fast_pve_1 -o $VBMDIR/GM_MNI152_1mm -w $VBMDIR/T1.anat/T1_to_MNI_nonlin_field
					run applywarp -r $FSLDIR/data/standard/MNI152_T1_2mm_brain -i $VBMDIR/T1.anat/T1_fast_pve_1 -o $VBMDIR/GM_MNI152_2mm -w $VBMDIR/T1.anat/T1_to_MNI_nonlin_field
					# modulation of gray matter segment in 1mm
					run fslmaths $VBMDIR/T1.anat/GM_MNI152_1mm -mul $VBMDIR/T1.anat/T1_to_MNI_nonlin_jac $VBMDIR/T1.anat/GM_MNI152_1mm_mod -odt float
					run fslmaths $VBMDIR/T1.anat/GM_MNI152_1p25mm -mul $VBMDIR/T1.anat/T1_to_MNI_nonlin_jac $VBMDIR/T1.anat/GM_MNI152_1p25mm_mod -odt float
					# smooth modulated
					run fslmaths $VBMDIR/GM_MNI152_1mm_mod -s 3 $VBMDIR/GM_MNI152_1mm_mod_s3
					run fslmaths $VBMDIR/GM_MNI152_1p25mm_mod -s 3 $VBMDIR/GM_MNI152_1p25mm_mod_s3
					# warp WM 2mm
					run applywarp -r $FSLDIR/data/standard/MNI152_T1_2mm_brain -i $VBMDIR/T1.anat/T1_fast_pve_2 -o $VBMDIR/WM_MNI152_2mm -w $VBMDIR/T1.anat/T1_to_MNI_nonlin_field
					# warp CSF 2mm
					run applywarp -r $FSLDIR/data/standard/MNI152_T1_2mm_brain -i $VBMDIR/T1.anat/T1_fast_pve_0 -o $VBMDIR/CSF_MNI152_2mm -w $VBMDIR/T1.anat/T1_to_MNI_nonlin_field
					# warp PVE 1mm & 2mm
					run applywarp -r $FSLDIR/data/standard/MNI152_T1_1mm_brain -i $VBMDIR/T1.anat/T1_fast_pveseg -o $VBMDIR/pveseg_MNI152_1mm -w $VBMDIR/T1.anat/T1_to_MNI_nonlin_field --interp=nn
					run applywarp -r $FSLDIR/data/standard/MNI152_T1_2mm_brain -i $VBMDIR/T1.anat/T1_fast_pveseg -o $VBMDIR/pveseg_MNI152_2mm -w $VBMDIR/T1.anat/T1_to_MNI_nonlin_field --interp=nn
					# warp subcortical segments 1mm & 2mm	?? usefull ?? which resolution ??
					run applywarp -r $FSLDIR/data/standard/MNI152_T1_1mm_brain -i $VBMDIR/T1.anat/T1_subcort_seg -o $VBMDIR/subcort_seg_MNI152_1mm -w $VBMDIR/T1.anat/T1_to_MNI_nonlin_field --interp=nn
					run applywarp -r $FSLDIR/data/standard/MNI152_T1_2mm_brain -i $VBMDIR/T1.anat/T1_subcort_seg -o $VBMDIR/subcort_seg_MNI152_2mm -w $VBMDIR/T1.anat/T1_to_MNI_nonlin_field --interp=nn
					date >> $subLog; echo "	# T1 processing finished"	>> $subLog
				fi
		else
			date >> $subLog; echo "	# No T1 image in $T1DIR found, exiting.."	>> $subLog
		fi
	fi

}


function T2_anat	{

	if [ -f $VBMDIR/T1_T2_GM_mod_MNI152_s3.nii.gz ]; then
		date >> $subLog; echo "	- T2 Bias field correction & tissue segmentation ready"	>> $subLog
	else
		if [ -f $T2RAW ]; then
				if [ -f $VBMDIR/T2.anat/T2_biascorr_brain.nii.gz ]; then
					date >> $subLog; echo "	- T2 Bias field correction & tissue segmentation ready"	>> $subLog
				else
					run mkdir -p $VBMDIR
					date >> $subLog; echo "	T2 Bias field correction & tissue segmentation"	>> $subLog
					run fsl_anat -t T2 --noreg --nononlinreg --nosubcortseg --clobber -i $T2RAW -o $VBMDIR/T2
				fi

				if [ -f $VBMDIR/T2.anat/T1_T2_mixeltype.nii.gz ]; then
					date >> $subLog; echo "	- Multichannel segmentation ready"	>> $subLog
				else
					date >> $subLog; echo "	Multichannel segmentation"	>> $subLog
					run flirt -in $VBMDIR/T2.anat/T2_biascorr_brain -ref $VBMDIR/T1.anat/T1_biascorr_brain -out $VBMDIR/T2.anat/T2_brain_to_T1 -cost corratio -searchrx -180 180 -searchry -180 180 -searchrz -180 180 -dof 6 -interp spline
					# create linear MNI to T1
					run convert_xfm -inverse -omat $VBMDIR/T1.anat/MNI_to_T1_lin.mat $VBMDIR/T1.anat/T1_to_MNI_lin.mat
					# multichannel segmentation using bias-corrected T1 & T2
					run fast -S 2 -g -a $VBMDIR/T1.anat/MNI_to_T1_lin.mat --Prior -o $VBMDIR/T2.anat/T1_T2 $VBMDIR/T1.anat/T1_biascorr_brain $VBMDIR/T2.anat/T2_brain_to_T1
				fi

				if [ ! -f $VBMDIR/T2_brain.nii.gz ]; then
					# write tissue segment volumes to file
					GMV=$(fslstats $VBMDIR/T2.anat/T1_T2_pve_1 -M -V | awk '{ print $1 * $3 }')
					WMV=$(fslstats $VBMDIR/T2.anat/T1_T2_pve_0 -M -V | awk '{ print $1 * $3 }')
					CSFV=$(fslstats $VBMDIR/T2.anat/T1_T2_pve_2 -M -V | awk '{ print $1 * $3 }')
					run echo -e "Vol_T1T2 \t $GMV \t $WMV \t $CSFV" >> $VBMDIR/vol_GM_WM_CSF.txt
					# gather usefull files
					run immv $VBMDIR/T2 $VBMDIR/T2_orig
					run imcp $VBMDIR/T2.anat/T2_brain_to_T1 $VBMDIR/T2_brain
					run imcp $VBMDIR/T2.anat/T2_biascorr $VBMDIR/T2
				fi

				if [ ! -f $VBMDIR/T1_T2_GM_mod_MNI152_s3.nii.gz ]; then
					date >> $subLog; echo "	Modulation of multi channel gray matter segments"	>> $subLog
			 		# modulation of gray matter segment
					run fslmaths $VBMDIR/T2.anat/T1_T2_pve_1 -mul $VBMDIR/T1.anat/T1_to_MNI_nonlin_jac__to_T1 $VBMDIR/T2.anat/T1_T2_pve_1_m -odt float
					run applywarp -r $FSLDIR/data/standard/MNI152_T1_1mm_brain -i $VBMDIR/T2.anat/T1_T2_pve_1_m -w $VBMDIR/T1.anat/T1_to_MNI_nonlin_field -o $VBMDIR/T1_T2_GM_mod_MNI152
					### smooth modulated GM
					run fslmaths $VBMDIR/T1_T2_GM_mod_MNI152 -s 3 $VBMDIR/T1_T2_GM_mod_MNI152_s3
					date >> $subLog; echo "	# T2 processing finished"	>> $subLog
				fi
		else
			date >> $subLog; echo "	# No T2 image found (it's not $T2RAW) "	>> $subLog
		fi
	fi

}


function QC_T1	{

	if [ -f $VBMDIR/T1_MNI152_2mm.nii.gz ]; then
		if [ ! -f $QC/struct/$1_T1-MNI152.png ] || [ $QCnew = yes ]; then
			mkdir -p $QC/struct/
			date >> $subLog; echo "QC: T1 brain extraction;"	>> $subLog
			### QC: brain extraction # creation of png images for quality control
			slicer $VBMDIR/T1.anat/T1_biascorr $VBMDIR/T1.anat/T1_biascorr_brain -s 2 -x 0.35 $VBMDIR/sla.png -x 0.45 $VBMDIR/slb.png -x 0.55 $VBMDIR/slc.png -x 0.65 $VBMDIR/sld.png -y 0.35 $VBMDIR/sle.png -y 0.45 $VBMDIR/slf.png -y 0.55 $VBMDIR/slg.png -y 0.65 $VBMDIR/slh.png -z 0.35 $VBMDIR/sli.png -z 0.45 $VBMDIR/slj.png -z 0.55 $VBMDIR/slk.png -z 0.65 $VBMDIR/sll.png
			pngappend $VBMDIR/sla.png + $VBMDIR/slb.png + $VBMDIR/slc.png + $VBMDIR/sld.png + $VBMDIR/sle.png + $VBMDIR/slf.png + $VBMDIR/slg.png + $VBMDIR/slh.png + $VBMDIR/sli.png + $VBMDIR/slj.png + $VBMDIR/slk.png + $VBMDIR/sll.png $VBMDIR/brain_extraction1.png
			slicer $VBMDIR/T1.anat/T1_biascorr_brain $VBMDIR/T1.anat/T1_biascorr -s 2 -x 0.35 $VBMDIR/sla.png -x 0.45 $VBMDIR/slb.png -x 0.55 $VBMDIR/slc.png -x 0.65 $VBMDIR/sld.png -y 0.35 $VBMDIR/sle.png -y 0.45 $VBMDIR/slf.png -y 0.55 $VBMDIR/slg.png -y 0.65 $VBMDIR/slh.png -z 0.35 $VBMDIR/sli.png -z 0.45 $VBMDIR/slj.png -z 0.55 $VBMDIR/slk.png -z 0.65 $VBMDIR/sll.png
			pngappend $VBMDIR/sla.png + $VBMDIR/slb.png + $VBMDIR/slc.png + $VBMDIR/sld.png + $VBMDIR/sle.png + $VBMDIR/slf.png + $VBMDIR/slg.png + $VBMDIR/slh.png + $VBMDIR/sli.png + $VBMDIR/slj.png + $VBMDIR/slk.png + $VBMDIR/sll.png $VBMDIR/brain_extraction2.png
			pngappend $VBMDIR/brain_extraction1.png - $VBMDIR/brain_extraction2.png $QC/struct/$1_T1_brainX.png
			rm -f  $VBMDIR/sl?.png  $VBMDIR/brain_extraction1.png  $VBMDIR/brain_extraction2.png
			### QC: brain extraction #2 creation of png images for quality control
			slicer $VBMDIR/T1.anat/T1_biascorr $VBMDIR/T1.anat/T1_biascorr_brain -s 2 -x 0.25  $VBMDIR/sla.png -x 0.45  $VBMDIR/slb.png -x 0.6  $VBMDIR/slc.png -x 0.75  $VBMDIR/sld.png -y 0.25  $VBMDIR/sle.png -y 0.45  $VBMDIR/slf.png -y 0.6  $VBMDIR/slg.png -y 0.75  $VBMDIR/slh.png -y 0.85  $VBMDIR/slm.png -z 0.45  $VBMDIR/sli.png -z 0.6  $VBMDIR/slj.png -z 0.75  $VBMDIR/slk.png -z 0.85  $VBMDIR/sll.png
			pngappend  $VBMDIR/sla.png +  $VBMDIR/slb.png +  $VBMDIR/slc.png +  $VBMDIR/sld.png +  $VBMDIR/sle.png +  $VBMDIR/slf.png  $VBMDIR/brain_extraction1.png
			pngappend  $VBMDIR/sli.png +  $VBMDIR/slj.png +  $VBMDIR/slk.png +  $VBMDIR/sll.png +  $VBMDIR/slg.png +  $VBMDIR/slh.png +  $VBMDIR/slm.png  $VBMDIR/brain_extraction2.png
			pngappend  $VBMDIR/brain_extraction1.png -  $VBMDIR/brain_extraction2.png  $QC/struct/$1_T1_brainX2.png
			rm -f  $VBMDIR/sl?.png  $VBMDIR/brain_extraction1.png  $VBMDIR/brain_extraction2.png
			# QC: T1 to MNI warp # creation of png images for quality control
			slicer $VBMDIR/T1_MNI152_2mm $FSLDIR/data/standard/MNI152_T1_2mm_brain -s 2 -x 0.35 $VBMDIR/sla.png -x 0.45 $VBMDIR/slb.png -x 0.55 $VBMDIR/slc.png -x 0.65 $VBMDIR/sld.png -y 0.35 $VBMDIR/sle.png -y 0.45 $VBMDIR/slf.png -y 0.55 $VBMDIR/slg.png -y 0.65 $VBMDIR/slh.png -z 0.35 $VBMDIR/sli.png -z 0.45 $VBMDIR/slj.png -z 0.55 $VBMDIR/slk.png -z 0.65 $VBMDIR/sll.png
			pngappend $VBMDIR/sla.png + $VBMDIR/slb.png + $VBMDIR/slc.png + $VBMDIR/sld.png + $VBMDIR/sle.png + $VBMDIR/slf.png + $VBMDIR/slg.png + $VBMDIR/slh.png + $VBMDIR/sli.png + $VBMDIR/slj.png + $VBMDIR/slk.png + $VBMDIR/sll.png $VBMDIR/T1_to_MNI_warped1.png
			slicer $FSLDIR/data/standard/MNI152_T1_2mm_brain $VBMDIR/T1_MNI152_2mm -s 2 -x 0.35 $VBMDIR/sla.png -x 0.45 $VBMDIR/slb.png -x 0.55 $VBMDIR/slc.png -x 0.65 $VBMDIR/sld.png -y 0.35 $VBMDIR/sle.png -y 0.45 $VBMDIR/slf.png -y 0.55 $VBMDIR/slg.png -y 0.65 $VBMDIR/slh.png -z 0.35 $VBMDIR/sli.png -z 0.45 $VBMDIR/slj.png -z 0.55 $VBMDIR/slk.png -z 0.65 $VBMDIR/sll.png
			pngappend $VBMDIR/sla.png + $VBMDIR/slb.png + $VBMDIR/slc.png + $VBMDIR/sld.png + $VBMDIR/sle.png + $VBMDIR/slf.png + $VBMDIR/slg.png + $VBMDIR/slh.png + $VBMDIR/sli.png + $VBMDIR/slj.png + $VBMDIR/slk.png + $VBMDIR/sll.png $VBMDIR/T1_to_MNI_warped2.png
			pngappend $VBMDIR/T1_to_MNI_warped1.png - $VBMDIR/T1_to_MNI_warped2.png $QC/struct/$1_T1-MNI152.png
			rm -f $VBMDIR/sl?.png $VBMDIR/T1_to_MNI_warped1.png $VBMDIR/T1_to_MNI_warped2.png
		fi
	fi

}


function QC_T2	{

	if [ -f $VBMDIR/T2_brain.nii.gz ]; then
		if [ ! -f $QC/struct/$1_T2_brainX2.png ] || [ $QCnew = yes ]; then
			date >> $subLog; echo "QC: T2 brain extraction"	>> $subLog
			### QC: brain extraction # creation of png images for quality control
			slicer $VBMDIR/T2.anat/T2_biascorr $VBMDIR/T2.anat/T2_biascorr_brain -s 2 -x 0.35 $VBMDIR/sla.png -x 0.45 $VBMDIR/slb.png -x 0.55 $VBMDIR/slc.png -x 0.65 $VBMDIR/sld.png -y 0.35 $VBMDIR/sle.png -y 0.45 $VBMDIR/slf.png -y 0.55 $VBMDIR/slg.png -y 0.65 $VBMDIR/slh.png -z 0.35 $VBMDIR/sli.png -z 0.45 $VBMDIR/slj.png -z 0.55 $VBMDIR/slk.png -z 0.65 $VBMDIR/sll.png
			pngappend $VBMDIR/sla.png + $VBMDIR/slb.png + $VBMDIR/slc.png + $VBMDIR/sld.png + $VBMDIR/sle.png + $VBMDIR/slf.png + $VBMDIR/slg.png + $VBMDIR/slh.png + $VBMDIR/sli.png + $VBMDIR/slj.png + $VBMDIR/slk.png + $VBMDIR/sll.png $VBMDIR/brain_extraction1.png
			slicer $VBMDIR/T2.anat/T2_biascorr_brain $VBMDIR/T2.anat/T2_biascorr -s 2 -x 0.35 $VBMDIR/sla.png -x 0.45 $VBMDIR/slb.png -x 0.55 $VBMDIR/slc.png -x 0.65 $VBMDIR/sld.png -y 0.35 $VBMDIR/sle.png -y 0.45 $VBMDIR/slf.png -y 0.55 $VBMDIR/slg.png -y 0.65 $VBMDIR/slh.png -z 0.35 $VBMDIR/sli.png -z 0.45 $VBMDIR/slj.png -z 0.55 $VBMDIR/slk.png -z 0.65 $VBMDIR/sll.png
			pngappend $VBMDIR/sla.png + $VBMDIR/slb.png + $VBMDIR/slc.png + $VBMDIR/sld.png + $VBMDIR/sle.png + $VBMDIR/slf.png + $VBMDIR/slg.png + $VBMDIR/slh.png + $VBMDIR/sli.png + $VBMDIR/slj.png + $VBMDIR/slk.png + $VBMDIR/sll.png $VBMDIR/brain_extraction2.png
			pngappend $VBMDIR/brain_extraction1.png - $VBMDIR/brain_extraction2.png $QC/struct/$1_T2_brainX.png
			rm -f  $VBMDIR/sl?.png  $VBMDIR/brain_extraction1.png  $VBMDIR/brain_extraction2.png
			### QC: brain extraction #2 creation of png images for quality control
			slicer $VBMDIR/T2.anat/T2_biascorr $VBMDIR/T2.anat/T2_biascorr_brain -s 2 -x 0.25  $VBMDIR/sla.png -x 0.45  $VBMDIR/slb.png -x 0.6  $VBMDIR/slc.png -x 0.75  $VBMDIR/sld.png -y 0.25  $VBMDIR/sle.png -y 0.45  $VBMDIR/slf.png -y 0.6  $VBMDIR/slg.png -y 0.75  $VBMDIR/slh.png -y 0.85  $VBMDIR/slm.png -z 0.45  $VBMDIR/sli.png -z 0.6  $VBMDIR/slj.png -z 0.75  $VBMDIR/slk.png -z 0.85  $VBMDIR/sll.png
			pngappend  $VBMDIR/sla.png +  $VBMDIR/slb.png +  $VBMDIR/slc.png +  $VBMDIR/sld.png +  $VBMDIR/sle.png +  $VBMDIR/slf.png  $VBMDIR/brain_extraction1.png
			pngappend  $VBMDIR/sli.png +  $VBMDIR/slj.png +  $VBMDIR/slk.png +  $VBMDIR/sll.png +  $VBMDIR/slg.png +  $VBMDIR/slh.png +  $VBMDIR/slm.png  $VBMDIR/brain_extraction2.png
			pngappend  $VBMDIR/brain_extraction1.png -  $VBMDIR/brain_extraction2.png  $QC/struct/$1_T2_brainX2.png
			rm -f  $VBMDIR/sl?.png  $VBMDIR/brain_extraction1.png  $VBMDIR/brain_extraction2.png
		fi
	fi

}

date
echo "Start 3D preprocessing of: $1"
for subject in $1;	do
			prepro $subject
done
