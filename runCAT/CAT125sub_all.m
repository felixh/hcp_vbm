function CAT125sub_all(sub_raw, xsub, targetdir, force)
%%%
%       CAT12 segmentation using SHOOTING & DARTEL
%
%       Felix Hoffstaedter
%       Data and Platforms
%       INM-7 - Brain and Behaviour
%       Forschungszentrum Jülich
%
%       CATsub_all( <subjectfolder>, <subjectname>, <targetdirectory>, <force processing> )
%%%

warning off all

% define path to spm12 containing cat12.5 toolbox
addpath /data/TOOLS/spm12_cat12.5

% --------------- start script only if T1w exists ---------------
if exist(sub_raw,'file')

% --- how many subjects are processed in parallel [in window = 0 ] --
jobs  = 0;    %  a job will use min 4 cores for i.e. denoising
% --- how many cores are use for fractal dimension estimation      --
cores  = 0;   %  1 if multiple subjects are processed
% ------- smoothing of modulated volume data im mm [5 & 8 default] --
vbmFWHM = [5 8];
% ---------- estimate fractal dimensions - SLOW !  ------------------
FD      = 1;
% ---------- smoothing of cortical thickness in mm [>=15] -----------
thkFWHM = [0 15];
% ---------- smoothing of folding parameters in mm [>=25] -----------
srfFWHM = [0 25];
%--------------- load cat12 expert defaults -------------------------
def = fullfile(spm_file(which('CAT125sub_all'), 'path'), 'cat125_defaults_bnb');
try global cat; if cat.extopts.expertgui == 0; spm fmri; cat12(def); end
catch;  spm fmri; cat12(def); end

sub_nii  = fullfile(targetdir, [xsub '.nii']);          %   Subjects T1 file for VBM

% --- delete old data if force = true ---
if  force == 1
    try rmdir(targetdir,'s'); end
end

if ~exist(sub_nii,'file')
    % ----------  copy & extract nifti file to CAT dir -----------------
    mkdir(targetdir);

    if strcmp(spm_file(sub_raw,'ext'),'gz')
        copyfile(sub_raw, fullfile(targetdir, [xsub '.nii.gz']));
        gunzip(fullfile(targetdir, [xsub '.nii.gz']));
        delete(fullfile(targetdir, [xsub '.nii.gz']));
    elseif strcmp(spm_file(sub_raw,'ext'),'nii')
        copyfile(sub_raw, sub_nii);
    else
        return
    end

    % for robustness set (0 0 0) coordinate to middle of image
    V = spm_vol(sub_nii);
    % pre-estimated COM of MNI template
    com_reference = [0 -20 -15];
    Affine = eye(4);
    vol = spm_read_vols(V);
    avg = mean(vol(:));
    avg = mean(vol(vol>avg));
    % don't use background values
    [x,y,z] = ind2sub(size(vol),find(vol>avg));
    com = V.mat(1:3,:)*[mean(x) mean(y) mean(z) 1]';
    com = com';
    M = spm_get_space(V.fname);
    Affine(1:3,4) = (com - com_reference)';
    spm_get_space(V.fname,Affine\M);

end

% ---------- high-dimensional SHOOTING & DARTEL normalization -----------------
if ~exist(fullfile(targetdir, 'mri', ['m0wp1' xsub '.nii']), 'file') || ...
        ~exist(fullfile(targetdir, 'report', ['cat_' xsub '.xml']), 'file') || force == 1
    matlabbatch = {};
    if exist(fullfile(targetdir, 'surf', ['lh.central.' xsub '.gii']),'file')
        matlabbatch{1}.spm.tools.cat.estwrite.output.surface = 0;
    end
    matlabbatch{1}.spm.tools.cat.estwrite.nproc = jobs;
    % data to process
    matlabbatch{1}.spm.tools.cat.estwrite.data{1} = [sub_nii ',1'];
    % run things
    spm_jobman('run', matlabbatch)
end

% ---------- Smooth modulated gray & white matter segments --------------------
if ~exist(fullfile(targetdir, 'mri', ['s' num2str(vbmFWHM(numel(vbmFWHM))) 'm0wp1' xsub '.nii']),'file') ...
        || force == 1
    matlabbatch = {};
    matlabbatch{1}.spm.spatial.smooth.data = {
                               fullfile(targetdir, 'mri', ['mwp1' xsub '.nii'])
                               fullfile(targetdir, 'mri', ['m0wp1' xsub '.nii'])};
    for i = 1:numel(vbmFWHM)
        matlabbatch{1}.spm.spatial.smooth.fwhm = [vbmFWHM(i) vbmFWHM(i) vbmFWHM(i)];
        matlabbatch{1}.spm.spatial.smooth.prefix = ['s' num2str(vbmFWHM(i))];
        spm_jobman('run', matlabbatch)
    end
end

% ---------- Estimate total intracranial volume (TIV) & -------------------
% ---------- global GM, WM, CSF, WM-hyperintensity vol --------------------
if (~exist(fullfile(targetdir, ['TIV_' xsub '.txt']),'file') || force == 1) && ...
  exist(fullfile(targetdir, 'report', ['cat_' xsub '.xml']), 'file')
    matlabbatch = {};
    matlabbatch{1}.spm.tools.cat.tools.calcvol.data_xml = ...
    {fullfile(targetdir, 'report', ['cat_' xsub '.xml'])};
    matlabbatch{1}.spm.tools.cat.tools.calcvol.calcvol_TIV = 0;
    matlabbatch{1}.spm.tools.cat.tools.calcvol.calcvol_name = [ 'TIV_' xsub '.txt'];
    spm_jobman('run', matlabbatch);
    movefile(fullfile(pwd, ['TIV_' xsub '.txt']), fullfile(targetdir, ['TIV_' xsub '.txt']))
end

if exist(fullfile(targetdir, 'surf', ['lh.central.' xsub '.gii']),'file')
    % ---------- Extract gyrification, cortical complexity and sulcus depth  --------------------
    if ~exist(fullfile(targetdir, 'surf', ['rh.sqrtsulc.' xsub]),'file') || force == 1
        matlabbatch = {};
        matlabbatch{1}.spm.tools.cat.stools.surfextract.data_surf = {
                                fullfile(targetdir, 'surf', ['lh.central.' xsub '.gii'])};
        matlabbatch{1}.spm.tools.cat.stools.surfextract.GI = 1;
        matlabbatch{1}.spm.tools.cat.stools.surfextract.FD = FD;
        matlabbatch{1}.spm.tools.cat.stools.surfextract.SD = 1;
        matlabbatch{1}.spm.tools.cat.stools.surfextract.nproc = cores;
        spm_jobman('run', matlabbatch)
    end

    % ---------- Resample and smooth cortical thickness  --------------------
    if ~exist(fullfile(targetdir, 'surf', ['s' num2str(thkFWHM(numel(thkFWHM))) '.rh.thickness.resampled.' xsub '.gii']),'file') ...
            || force == 1
        matlabbatch = {};
        matlabbatch{1}.spm.tools.cat.stools.surfresamp.data_surf = {
                                fullfile(targetdir, 'surf', ['lh.thickness.' xsub])};
        matlabbatch{1}.spm.tools.cat.stools.surfresamp.nproc = cores;
        for i = 1:numel(thkFWHM)
            matlabbatch{1}.spm.tools.cat.stools.surfresamp.fwhm_surf = thkFWHM(i);
            matlabbatch{1}.spm.tools.cat.stools.surfresamp.merge_hemi = 0;
            matlabbatch{1}.spm.tools.cat.stools.surfresamp.mesh32k = 0;
            spm_jobman('run', matlabbatch)
            matlabbatch{1}.spm.tools.cat.stools.surfresamp.merge_hemi = 1;
            matlabbatch{1}.spm.tools.cat.stools.surfresamp.mesh32k = 1;
            spm_jobman('run', matlabbatch)
        end
    end

    % ---------- Resample and smooth gyrification and sulcus depth  --------------------
    if ~exist(fullfile(targetdir, 'surf', ['s' num2str(srfFWHM(numel(srfFWHM))) '.rh.sqrtsulc.resampled.' xsub '.gii']),'file') ...
            || force == 1
        matlabbatch = {};
        if FD == 0
            matlabbatch{1}.spm.tools.cat.stools.surfresamp.data_surf = {
                                    fullfile(targetdir, 'surf', ['lh.gyrification.' xsub])
                                    fullfile(targetdir, 'surf', ['lh.sqrtsulc.' xsub])};
        else
            matlabbatch{1}.spm.tools.cat.stools.surfresamp.data_surf = {
                                    fullfile(targetdir, 'surf', ['lh.gyrification.' xsub])
                                    fullfile(targetdir, 'surf', ['lh.sqrtsulc.' xsub])
                                    fullfile(targetdir, 'surf', ['lh.fractaldimension.' xsub])};
        end
        matlabbatch{1}.spm.tools.cat.stools.surfresamp.nproc = cores;
        for i = 1:numel(srfFWHM)
            matlabbatch{1}.spm.tools.cat.stools.surfresamp.fwhm_surf = srfFWHM(i);
            matlabbatch{1}.spm.tools.cat.stools.surfresamp.merge_hemi = 0;
            matlabbatch{1}.spm.tools.cat.stools.surfresamp.mesh32k = 0;
            spm_jobman('run', matlabbatch)
            matlabbatch{1}.spm.tools.cat.stools.surfresamp.merge_hemi = 1;
            matlabbatch{1}.spm.tools.cat.stools.surfresamp.mesh32k = 1;
            spm_jobman('run', matlabbatch)
        end
    end
end

% delete(sub_nii);

exit

end
end
