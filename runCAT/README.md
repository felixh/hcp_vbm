Wrapper to do excessive T1w structural processing using

Computational Anatomy Toolbox
CAT12.X - http://www.neuro.uni-jena.de
by Christian Gaser & Robert Dahnke

including SLURM job submission

defaults file:            cat125_defaults_bnb.m / cat126_defaults_bnb.m

single subject pipeline:  CAT125sub_all.m / CAT126sub_all.m

sample specific call script with BIDS parser: runCAT125_<sample>.m
