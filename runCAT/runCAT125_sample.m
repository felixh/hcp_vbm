clear, clc; warning off all
%%%
%       CAT12 segmentation using SHOOTING & DARTEL
%
%       Felix Hoffstaedter
%       Data and Platforms
%       INM-7 - Brain and Behaviour
%       Forschungszentrum Jülich
%
%       calling: CAT125sub_all( <path2raw.nii>, <subjectname(_session)>, <targetdir>, force )
%%%

% ----------  define sample to process   ----------------------------
sample = 'runTEST';
% force  --- reprocessing of ready subjects [0 = NOT reprocess] -----
force = 0;

% ------  define slurm call with max RAM & wall clock time   -------
call_slurm = ['srun -p large -N 1 -n 1 --mem-per-cpu=5500 --time=9:00:00 --job-name=' sample];
call_matlab = ' /nfsusr/local/MATLAB/R2016a/bin/matlab -nodisplay -nodesktop -nosplash -singleCompThread -r';


% -------  define source folder to get subject and sessions  --------
Dirs = fullfile('/data/Raw_Data', sample);
% ---------- CAT output folder  -------------------------------------
target = fullfile('/data/Derivatives/CAT/12.5/', sample);

Si = dir(fullfile(Dirs,'*')); Si = Si([Si.isdir]); Si = Si(3:end);

if strfind(Si(1).name,'sub')
    Di = Si;
    fprintf('Sample "%s" contains ~%d subjects \n', sample, size(Di,1));
    cnt=1;
    for i=1:size(Di,1)
        if strfind(Di(i).name,'sub')
            subdir = fullfile(Dirs, Di(i).name);
            Dii = dir(fullfile(subdir,'*'));
            try Dii = Dii([Dii.isdir]); Dii = Dii(3:end); end
            if strfind(Dii(1).name,'ses')
                for ii=1:size(Dii,1)
                    if exist(fullfile(subdir, Dii(ii).name, 'anat'),'dir')
                        fil = dir(fullfile(subdir, Dii(ii).name, 'anat', '*T1w.nii.gz'));
                        if numel(fil) > 0
                            sub_raw{cnt} = fullfile(subdir, Dii(ii).name, 'anat', fil(1).name);
                            xsubs{cnt} = [Di(i).name '_' Dii(ii).name];
                            targetdir{cnt} = fullfile(target, Di(i).name, Dii(ii).name);
                            cnt=cnt+1;
                        end
                    end
                end
            else
                if exist(fullfile(subdir, 'anat'),'dir')
                    fil = dir(fullfile(subdir, 'anat', '*T1w.nii.gz'));
                    if numel(fil) > 0
                        xsubs{cnt} = Di(i).name;
                        sub_raw{cnt} = fullfile(subdir, 'anat', fil(1).name);
                        targetdir{cnt} = fullfile(target, Di(i).name);
                        cnt=cnt+1;
                    end
                end
            end
        end
    end
else
    fprintf('%s Sites found \n', num2str(size(Si,1)));
    cnt=1;
    for s = 1:size(Si,1)
        site = Si(s).name;
        Di = dir(fullfile(Dirs, site, '*')); Di = Di([Di.isdir]); Di = Di(3:end);
        fprintf('%s: %d subjects \n', site, size(Di,1));
        for i = 1:size(Di,1)
            if strfind(Di(i).name,'sub')
                subdir = fullfile(Dirs, site , Di(i).name);
                Dii = dir(fullfile(subdir,'*'));
                try Dii = Dii([Dii.isdir]); Dii = Dii(3:end);
                    if strfind(Dii(1).name,'ses')
                        for ii = 1:size(Dii,1)
                            if exist(fullfile(subdir, Dii(ii).name, 'anat'),'dir')
                                fil = dir(fullfile(subdir, Dii(ii).name, 'anat', '*T1w.nii.gz'));
                                if numel(fil) > 0
                                    sub_raw{cnt} = fullfile(subdir, Dii(ii).name, 'anat', fil(1).name);
                                    xsubs{cnt} = [Di(i).name '_' Dii(ii).name];
                                    targetdir{cnt} = fullfile(target, site, Di(i).name, Dii(ii).name);
                                    cnt=cnt+1;
                                end
                            end
                        end
                    else
                        if exist(fullfile(subdir, 'anat'),'dir')
                            fil = dir(fullfile(subdir, 'anat', '*T1w.nii.gz'));
                            if numel(fil) > 0
                                xsubs{cnt} = Di(i).name;
                                sub_raw{cnt} = fullfile(subdir, 'anat', fil(1).name);
                                targetdir{cnt} = fullfile(target, site,  Di(i).name);
                                cnt=cnt+1;
                            end
                        end
                    end
                end
            end
        end
    end
end

% ----------  check for already processed subjects  -----------------------
idx= []; i=0;
if force
    idx = 1 : numel(xsubs);
else
    for sub = 1:numel(xsubs)
        if exist(sub_raw{sub}, 'file') && ...
                (~exist(fullfile(targetdir{sub}, 'surf', ['s25.rh.sqrtsulc.resampled.' xsubs{sub} '.gii']),'file') || ...
                            ~exist(fullfile(targetdir{sub}, ['TIV_' xsubs{sub} '.txt']),'file'))

            i = i + 1;
            idx(i) = sub;
        end
    end
end
if numel(idx) == 0
    fprintf('No subjects to process! Aborting.'); return;
else
    fprintf('Found %i unprocessed of %i subjects..\n', numel(idx), numel(xsubs));
end
sub_raw = sub_raw(idx);
targetdir = targetdir(idx);
xsubs = xsubs(idx);

pause

% ----------  submit slurm jobs  -----------------------
for sub = 1:numel(xsubs)
  fprintf('Submit CAT12 job for subject %s \n', xsubs{sub});
  system([call_slurm ' --error=slurm_log/' xsubs{sub} call_matlab ' "CAT125sub_all(''' sub_raw{sub} ''' ,' '''' ...
      xsubs{sub} ''' ,' '''' targetdir{sub} ''' ,' '' num2str(force) ' )" &' ],'-echo');
end

% CAT125sub_all(sub_raw{sub}, xsubs{sub}, targetdir{sub}, num2str(force))
